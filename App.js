import * as React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import FBLoginButton from './components/FBLoginButton'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  label: {
    fontSize: 16,
    fontWeight: 'normal',
    marginBottom: 48,
  },
})

function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>Welcome to the Facebook SDK for React Native!</Text>
      <FBLoginButton />
    </View>
  )
}

export default App
